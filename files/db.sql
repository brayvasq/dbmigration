
-- BORRADO DE TABLAS Y CONSTRAINTS
USE example;
DROP TABLE User;  -- CASCADE CONSTRAINTS;

--creacion tablas
CREATE TABLE User(
  first_name VARCHAR2(35),
  sex CHAR(1),
  country VARCHAR2(35)
);


INSERT INTO User VALUES ("Pepe", 'M', "COLOMBIA");
INSERT INTO User VALUES ("Jhon", 'M', "ENGLAND");
INSERT INTO User VALUES ("Jay", 'M', "USA");
INSERT INTO User VALUES ("Kylie", 'M', "USA");
INSERT INTO User VALUES ("Roma", 'M', "ITALIA");
INSERT INTO User VALUES ("Nacho", 'M', "MEXICO");
INSERT INTO User VALUES ("Homero", 'M', "USA");
INSERT INTO User VALUES ("Lucky", 'M', "IRELAND");

--fin creacion tablas
