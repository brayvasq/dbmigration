#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
from src.commands import Commands

if __name__ == "__main__":
    print("Info: Operative system " + sys.platform)
    Commands(sys.argv).process()
    