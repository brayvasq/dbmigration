# Python DB's project
Project to import data in multiple databases and show how to execute operations over differents databases.

> :warning: This project was created in the year 2015-1 with educational/study purposes only.

| Lenguaje | Versión        | SO                      |
| -------- | -------------- | -----------------       |
| Python   | Python 3.8.2   | Manjaro 20.0.1 Lysia    |

## Documentation

| Type          | Link                                           |
| ------------- | ---------------------------------------------- |
| MySQL         | https://dev.mysql.com/doc/connector-python/en/connector-python-example-connecting.html       |
| PostgreSQL    | https://www.psycopg.org/docs/usage.html#|

## DB's supported
- MySQL
- PostgreSQL
- MSSQL
- ORACLE

## Run project
```bash
# To run the project you will need to have docker installed and running
sudo systemctl start docker.service

# Setup environment
# Creating environment
virtualenv .


# Installing dependencies
# NOTE: For Postgresql you should have installed the libpq-dev lib in your system.
pip install -r requirements.txt

# Running mysql 
docker-compose -f docker/mysql.yml up

# Running project for mysql
python main.py db=mysql file=files/data.csv

# Running postgresql
docker-compose -f docker/postgresql.yml up

# Running project for postgresql
python main.py db=pg file=files/data.csv
```