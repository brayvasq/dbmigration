import re
import io
from src.factories.db_factory import DbFactory

class Commands:
    def __init__(self, data):
        """
        Parameters
        ----------
        data : list
            The input array
        """
        self.data = data
        self.data_len = len(self.data)

    def process(self):
        db = self.__get_param("db=", "([a-zA-Z]+)")
        path = self.__get_param("file=", "(.*)")

        if path:
            driver = DbFactory(db).get_driver()
            try:
                print("Info: File to import " + path)
                data = io.open(path)

                if data:
                    sentence = data.readlines()
                    for i in sentence:
                        i = i.rsplit('\n')

                    driver.process(sentence)
            except:
                print("Data couldn't be loaded")
        else:
            print("Error: Not file was found")

    def __get_param(self, value, data_type):
        """
        Returns the queried param using regex
        Parameters
        ----------
        value : str
            variable name to search
        data_type : str
            variable pattern equivalent
        Return
        ------
        param : str
            the param value if a match occurs
        None
            otherwise
        """
        param = None

        if self.data_len > 1:
            data_str = ';'.join(self.data[1:])
            param = re.search(value+data_type, data_str)

            if param:
                param = param.group(1).replace(value, '') if param.group(1) else None

        return param