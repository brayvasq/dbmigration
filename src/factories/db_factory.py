from src.utils.mysql_db import MysqlDB
from src.utils.postgres_db import PostgresDB
from src.utils.mssql_db import MssqlDB
from src.utils.oracle_db import OracleDB

class DbFactory:
    def __init__(self, db : str ="mysql"):
        """
        Parameters
        ----------
        animal_type : str
            The type of the animal to create (default: "cow").
        """
        self.type = db if db else "mysql"

    def get_driver(self):
        """
        Return a specific type of animal, depending of the type of animal
        Parameters
        ----------
        name : str
            the name of the animal to create
        Return
        ------
        animal
            A new specific animal
        """
        db = None
        if self.type == "mysql":
            db = MysqlDB()
        elif self.type == "pg":
            db = PostgresDB()
        elif self.type == "mssql"
            db = MssqlDB()
        elif self.type == "oracle"
            db = OracleDB()

        return db