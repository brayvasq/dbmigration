import pymssql
from os import path


class MssqlDB:
    """
    Util class to connect with SQL Server DB and import data.

    ...
    Attributes
    ----------
    connection
        SQL Server DB connection instance

    cursor
        Cursor to operate over the DB.

    Methods
    -------
    process()
        Excecute the process to import data.

    setup()
        Create a new sql server connection and return it.

    drop()
        Drop the users table.

    create()
        Create the users table.

    insert(data)
        Iterates the data to import and insert into the table users.

    tables()
        Show existing tables.

    records()
        Show records from users table.

    close()
        Close cursor and connection.
    """

    def __init__(self):
        print("Info: Driver SQL Server DB ")
        self.connection = self.setup()
        self.cursor = None

    def process(self, data):
        """
        Excecute the process to import data.

        Parameters
        ----------
        data : list
            List of data to import and insert into the users table.
        """
        if self.connection:
            self.cursor = self.connection.cursor()
            self.cursor.execute('use example;')
            self.drop()
            self.create()
            self.insert(data)
            self.tables()
            self.records()
        self.close()

    def setup(self):
        """
        Creates a new SQL Server DB connection.

        Return
        ------
        cnx
            New SQL Server DB connection.
        """
        print("\nInfo: Establishing connection....")

        cnx = None

        try:
            cnx = pymssql.connect(server='127.0.0.1', user='example', password='example', database='example')
        except Exception as e:
            print("\nError: Connection couldn't be established")
            print(e)

        return cnx


    def drop(self):
        """
        Drop table users if exists. Otherwise, prints an exception message.
        """
        print("\nInfo: Deleting table users")

        try:
            self.cursor.execute('drop table users')
            self.connection.commit()
        except Exception as e:
            print("\nError: Driver couldn't delete the table users.")
            print(e)

    def create(self):
        """
        Create table users if it doesn't exist. Otherwise, prints an exception message.
        """
        print("\nInfo: Creating table users")

        try:
            self.cursor.execute('create table users(first_name CHAR(35), sex CHAR(1), country CHAR(35))')
            
            self.connection.commit()
        except Exception as e:
            print("\nError: Driver couldn't create the table users.")
            print(e)

    def insert(self, data):
        """
        Iterate over data and insert it into the table users. Otherwise, prints an exception message.

        Parameters
        ----------
        data : list
            List of data to import and insert into the users table.
        """
        records = 0
        fail = 0
        
        print("\nInfo: Start records import")

        for item in data:
            try:
                attributes = item.split(';')

                sql = 'insert into users values (%(name)s, %(sex)s, %(country)s)'
                insercion = {'name': attributes[0], 'sex': attributes[1], 'country': attributes[2]}
                
                self.cursor.execute(sql, insercion)

                records += 1

                self.connection.commit()
            except Exception as e:
                print("\nError: A record couldn't be created.")
                print(e)
                fail += 1

        print("Info: Number of records " + str(records))
        print("Info: Succesfull records " + str(records - fail))
        print("Info: Failed records " + str(fail))
        print("Info: Records import is complete")

    def tables(self):
        """
        Show existing tables. Otherwise, prins an exception message.
        """
        print("\nInfo: Tables:")

        try:
            self.cursor.execute('SELECT Distinct TABLE_NAME FROM information_schema.TABLES')

            tables = self.cursor.fetchall()
            for (table,) in tables:
                print(table)
        except Exception as e:
            print("\nError: A tables couldn't be showed.")
            print(e)

    def records(self):
        """
        Show existing records in users table. Otherwise, prints an exception message.
        """
        print("\nInfo: Records:")

        try:
            self.cursor.execute('select * from users')

            records = self.cursor.fetchall()
            for record in records:
                print(record)
        except Exception as e:
            print("\nError: Records couldn't be showed.")
            print(e)

    def close(self):
        """
        Close cursor and connection. Otherwise, prints an exception message.
        """
        print("\nInfo: Closing connection....")

        try:
            self.cursor.close()
            self.connection.close()
        except Exception as e:
            print("\nError: Connection couldn't close.")
            print(e)
